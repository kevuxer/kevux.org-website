<!DOCTYPE html>
<html lang="en">
  <head>
    <title>News - 2024/10/01 - FLL 0.6.12 Release</title>

    <base href="../../">

    <meta charset="UTF-8">
    <meta name="author" content="Kevin Day">
    <meta name="description" content="News post on 2024/10/01.">
    <meta name="keywords" content="Kevin Day, Kevux, FLL, Featureless, Linux, Library, Distribution, Open-Source, News, 2024">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link type="text/css" rel="stylesheet" media="all" href="css/kevux.css">
    <link type="text/css" rel="stylesheet" media="only screen" href="css/kevux-screen.css">
    <link type="text/css" rel="stylesheet" media="only screen and (min-device-width:501px)" href="css/kevux-screen-desktop.css">
    <link type="text/css" rel="stylesheet" media="only screen and (max-device-width:500px)" href="css/kevux-screen-mobile.css">
    <link type="text/css" rel="stylesheet" media="only screen and (min-device-width:1201px)" href="css/kevux-screen-large.css">
    <link type="text/css" rel="stylesheet" media="only screen and (min-device-width:501px) and (max-device-width:1200px)" href="css/kevux-screen-normal.css">
    <link type="text/css" rel="stylesheet" media="only screen and (min-device-width:251px) and (max-device-width:500px)" href="css/kevux-screen-small.css">
    <link type="text/css" rel="stylesheet" media="only screen and (max-device-width:250px)" href="css/kevux-screen-tiny.css">
    <link type="text/css" rel="stylesheet" media="only print" href="css/kevux-print.css">
    <link type="text/css" rel="stylesheet" media="only print and (orientation:landscape)" href="css/kevux-print-landscape.css">
    <link type="text/css" rel="stylesheet" media="only print and (orientation:portrait)" href="css/kevux-print-portrait.css">

    <link rel="canonical" href="news/2024/2024_10_01-fll_0_6_12_release.html">
    <link type="image/x-icon" rel="icon" href="images/kevux.ico">
    <link type="image/x-icon" rel="shortcut" href="images/kevux.ico">
    <link type="text/html" rel="license" href="licenses.html">
    <link type="text/html" rel="prev" href="news/2024/2024_08_11-fll_0_6_11_release.html">
  </head>

  <body id="kevux" class="kevux no-js news">
    <div role="banner" class="header-block">
      <header class="header-section header">
        <div class="header-site">Kevux Systems and Software</div>
      </header>

      <div class="nav-block">
        <nav id="kevux-site-nav" class="nav-menu">
          <div class="nav-item active"><a href="news.html" class="nav-text link">News</a></div>
          <div class="nav-item"><a href="distributions.html" class="nav-text link">Distributions</a></div>
          <div class="nav-item"><a href="fll.html" class="nav-text link">FLL</a></div>
          <div class="nav-item"><a href="projects.html" class="nav-text link">Projects</a></div>
          <div class="nav-item"><a href="documentation.html" class="nav-text link">Documentation</a></div>
        </nav>
      </div>
    </div>

    <div class="content-block">
      <div id="nav-expanded" class="nav-block">
        <nav id="kevux-document-nav" class="nav-menu">
          <div class="nav-item block back">
            <a href="news/2024.html" class="nav-text link back">Back</a>
          </div>
          <div class="nav-item block highlight unlink">
            <div class="nav-text notice">2024 / 10 / 01</div>
            <div class="nav-text unlink">FLL 0.6.12 Release</div>
          </div>
          <div class="nav-item block ellipses">
            <a href="news/2024/2024_10_01-fll_0_6_12_release.html#nav-expanded" class="nav-text link open" title="Expand Menu">…</a>
            <a href="news/2024/2024_10_01-fll_0_6_12_release.html" class="nav-text link close">Collapse Menu</a>
          </div>
        </nav>
      </div>

      <div role="document" class="main-block">
        <main class="main">
          <header class="section-header header">
            <h1 class="section-title h h1">2024 / 10 / 01 - FLL 0.6.12 Release</h1>
          </header>

          <div class="main-content">
            <p class="p">
              The <strong class="strong">Featureless Linux Library</strong> stable version <code class="code">0.6.12</code> is released.
            </p>
            <p class="p">
              This release provides some significant polishing, bug fixing, and corrections (or clarifications) to the project, the standards, and the specifications.
              Much of this work is done to pave the way for forward compatibility with improvements and changes introduced in the 0.7 development branches.
              I try to avoid making such changes in the specifications for stables releases but I feel these specific changes are merited.
            </p>
            <p class="p">
              This increases the correctness of the <abbr title="Featureless Settings Specifications">FSS</abbr> processing code, specifically with the <abbr title="Featureless Settings Specifications">FSS</abbr> read functions.
              The previous behavior only cared whether or not the tests produce equivalent results in terms of reading and writing.
              I back ported the <code class="code">0.7</code> runtime tests and found that it would be better to have consistent and correct output in regards to white space.
              Doing this also exposed several bugs and mistakes in the <abbr title="Featureless Settings Specifications">FSS</abbr> processing code.
            </p>
            <p class="p">
              The <strong class="strong">FSS Embedded Read</strong>, in particular, was completely forgotten about by me.
              The previous releases have been marked stable but I forgot to actually verify the proper functioning of the <strong class="strong">FSS Embedded Read</strong> program.
              The back porting of the runtime tests exposed significant problems with the <strong class="strong">FSS Embedded Read</strong> program.
            </p>
            <p class="p">
              The <strong class="strong">FSS Embedded Read</strong> to include the back ported fixes from the <code class="code">0.7</code> development branch:
            </p>
            <ul>
              <li>The use of <code class="code">f_memory_array_increase()</code> is incorrect in several cases.</li>
              <li>Switch to <code class="code">f_memory_array_resize()</code>.</li>
              <li>Add two when resizing to account for the depth position but also an additional element as a minor memory allocation optimization.</li>
              <li>The <code class="code">--select</code> now also follows the more strict behavior as implemented in the <code class="code">0.7</code> branch.</li>
              <li>The <code class="code">--total</code> to match the more strict behavior as implemented in the <code class="code">0.7</code> branch.</li>
            </ul>
            <p class="p">
              The <strong class="strong">FSS Embedded Read</strong> has the following additional notable fixes thanks to the runtime tests:
            </p>
            <ul>
              <li>Security issue regarding incorrect memset value (passed <code class="code">sizeof(skip)</code> when instead <code class="code">sizeof(bool)</code> should be used).</li>
              <li>The <code class="code">--line</code> parameter is not being correctly validated and throws an error when a value is specified.</li>
              <li>Fix a bug where empty Content is being improperly handled and <code class="code">}</code> is being returned as the Content for empty Content.</li>
              <li>The <code class="code">--select</code> is not always printing a number, so make sure a number, even if it is <code class="code">0</code>, is always printed.</li>
              <li>The comments are not being properly processed because <code class="code">graph_first</code> is not and should be being set to 0x1 at start, resulting in the first comment is not being treated as a comment.</li>
              <li>The entire Content is being displayed when <code class="code">--object</code> is used with <code class="code">--line</code> when instead only a single line must be printed.</li>
              <li>Column counts with empty data are not being correctly handled.</li>
              <li>Should not display anything when Content is empty and Objects are not being printed.</li>
              <li>Delimits are not being applied when printing in line mode.</li>
              <li>Do not double-print the before spaces when both original and trim modes are in use.</li>
              <li>Print the before even with original and trim are in use when the Object has no size.</li>
              <li>I had not implemented the <code class="code">--columns</code> support for <strong class="strong">FSS Embedded Read</strong> in prior releases and this is now properly supported.</li>
            </ul>
            <p class="p">
              The <strong class="strong">FSS-0002</strong> and <strong class="strong">FSS-0003</strong> standards have some unclear situations regarding the white space before and after a valid Object is now clarified.
              I discovered problems and realized that the standard could be more clear according to the spaces before and after the Object.
              This does not change the <strong class="strong">FSS-0002</strong> and <strong class="strong">FSS-0003</strong> standards in any functional way.
              The simply clarifies the standard regarding the spaces to make the specification more clear and reduce the chances for a mistake.
            </p>
            <p class="p">
              The original print is now better preserved.
              Printing an Object can now print the before and after Object characters (usually white space).
              The trimming and original print may be used together to produce the results where there is no white space before or after the Object but everything else from the original source is preserved.
              This combination of trim and original essentially ensures that the old behavior can be still produced.
              This does not utilize the <code class="code">closes</code> structure as is done in the 0.7 development branches to ensure <abbr title="Application Programming Interface">API</abbr> does not get broken.
              This behavior simulates the white space tabbing and more proper and precise support for this requires at least the 0.7 development branch.
            </p>
            <p class="p">
              I did some experimentation and found that I could get this project to build and run on an Android environment.
              I generally do not want OS-specific build files in the core <abbr title="Featureless Linux Library">FLL</abbr> project, but I have made an exception here.
              This should help build more support for the Android environment and help pave the way for future projects that may help those users unfortunate enough to find themselves in an Android environment.
              This Android functionality is not well tested nor is it well supported.
              The Android support has been tested to build using <code class="code">bootstrap.sh</code> and then using <code class="code">fake</code>.
            </p>
            <p class="p">
              Example build and install process on an Android:
                <pre class="preserve">
# mkdir ~/software
# cd fll-0.6.X/
# ./bootstrap.sh build -m monolithic -m thread -m clang -m android && ./install.sh ~/software
# cd ../fake-0.6.X/
# ./bootstrap.sh build -m monolithic -m thread -m clang -m android -w ~/software && ./install.sh ~/software
# export LD_LIBRARY_PATH=~/software/libraries/shared
# export PATH=~/software/programs/shared
# cd ../byte_dump-0.6.X/
# fake -m monolithic -m thread -m clang -m android -w ~/software && ./install.sh ~/software
</pre>
            </p>
            <p class="p">
              Example execution of <code class="code">byte_dump</code> of the <code class="code">bash</code> program (this is a large dump).
                <pre class="preserve">
# byte_dump -wt 7 $(type -p bash)
</pre>
            </p>
            <p class="p">
              The Featureless Make is intended to not be language specific since its inception.
              I did not know how the languages were to work when I first started this project, and so I mostly built this project to only guarantee C/C++ support.
            </p>
            <p class="p">
              I recently discovered that the <code class="code">golang</code> supports some command line based compilation.
              The <code class="code">golang</code> is now supported in the most basic way, but this support is not well tested.
              Use the <code class="code">example_go</code> project as a point of reference.
              I have only tested the basic build and I have not done anything more complex than that.
            </p>
            <p class="p">
              Some of the requirements in the build specification are now relaxed.
              The <code class="code">bash</code> is changed to <code class="code">shell</code> to encourage more types of scripts.
              The <code class="code">custom</code> is added and used as a fallback if <code class="code">build_language_path</code> is not used.
            </p>
            <p class="p">
              I found that there needs to be some additional settings in the <code class="code">settings</code> specification.
              This updates the specification and adds:
            </p>
            <ul>
              <li><code class="code">build_compiler_arguments</code></li>
              <li><code class="code">build_compiler_arguments_library</code></li>
              <li><code class="code">build_compiler_arguments_library_shared</code></li>
              <li><code class="code">build_compiler_arguments_library_static</code></li>
              <li><code class="code">build_compiler_arguments_object</code></li>
              <li><code class="code">build_compiler_arguments_object_shared</code></li>
              <li><code class="code">build_compiler_arguments_object_static</code></li>
              <li><code class="code">build_compiler_arguments_program</code></li>
              <li><code class="code">build_compiler_arguments_program_shared</code></li>
              <li><code class="code">build_compiler_arguments_program_static</code></li>
              <li><code class="code">build_compiler_arguments_shared</code></li>
              <li><code class="code">build_compiler_arguments_static</code></li>
              <li><code class="code">build_language_path</code></li>
            </ul>
            <p class="p">
              Some discovered mistakes in the language of the specification are also fixed.
            </p>
            <p class="p">
              The following are changes since the 0.6.11 stable release.
            </p>
            <p class="p">
              <strong class="strong">Exploit Fixes:</strong>
            </p>
            <ul>
              <li>None.</li>
            </ul>
            <p class="p">
              <strong class="strong">Security Fixes:</strong>
            </p>
            <ul>
              <li><strong class="strong">FLL:</strong> Incorrect memset value (passed <code class="code">sizeof(skip)</code> when instead <code class="code">sizeof(bool)</code> should be used).</li>
            </ul>
            <p class="p">
              <strong class="strong">Features:</strong>
            </p>
            <ul>
              <li><strong class="strong">FLL:</strong> Add support for building under Android.</li>
              <li><strong class="strong">Fake:</strong> Fake <code class="code">settings</code> specification updates and enable simple Golang support.</li>
            </ul>
            <p class="p">
              <strong class="strong">Bug Fixes:</strong>
            </p>
            <ul>
              <li><strong class="strong">FLL:</strong> Replace <code class="code">index()</code> with <code class="code">strchr()</code>.</li>
              <li><strong class="strong">FSS Read programs:</strong> FSS Embedded List Read printing tabbing when using original mode and trim mode.</li>
              <li><strong class="strong">FSS Read programs:</strong> Get the FSS Embedded List Read working as expected based on runtime tests.</li>
              <li><strong class="strong">FSS Read programs:</strong> FSS Embedded List Read is not applying delimits when printing in line mode.</li>
              <li><strong class="strong">FSS Read programs:</strong> Empty Content lines with no Object printed should not be included for FSS Embedded List.</li>
              <li><strong class="strong">FSS Read programs:</strong> Incorrect handling of column counts with empty data for FSS Embedded List Read.</li>
              <li><strong class="strong">FSS Read programs:</strong> FSS Embedded Read is printing a single line with <code class="code">--line</code> parameter with <code class="code">--object</code>.</li>
              <li><strong class="strong">FSS Read programs:</strong> Invalid comment handling in FSS Embedded Read.</li>
              <li><strong class="strong">FSS Read programs:</strong> FSS Embedded Read has incomplete and incorrect code.</li>
              <li><strong class="strong">FSS Read programs:</strong> FSS Extended Read needs to only print new line if Object or Content is printed.</li>
              <li><strong class="strong">FSS Read programs:</strong> FSS Extended Read needs to properly handle <code class="code">--total</code> when using <code class="code">--select</code> and also <code class="code">--empty</code>.</li>
              <li><strong class="strong">FSS Read programs:</strong> FSS Basic Read needs to properly handle <code class="code">--total</code> when using <code class="code">--select</code> and also <code class="code">--empty</code>.</li>
            </ul>
            <p class="p">
              <strong class="strong">Refactors:</strong>
            </p>
            <ul>
              <li>None.</li>
            </ul>
            <p class="p">
              <strong class="strong">Regressions:</strong>
            </p>
            <ul>
              <li>None.</li>
            </ul>
            <p class="p">
              <strong class="strong">Updates:</strong>
            </p>
            <ul>
              <li><strong class="strong">FLL:</strong> The bootstrap example script now handles building using <code class="code">fake</code>.</li>
              <li><strong class="strong">FLL:</strong> Improve robustness of the <code class="code">fll_control_group_prepare()</code> function.</li>
              <li><strong class="strong">FLL:</strong> The <abbr title="Featureless Settings Specifications">FSS</abbr> lists (<strong class="strong">FSS-0002</strong>, <strong class="strong">FSS-0003</strong>, <strong class="strong">FSS-0008</strong>, etc..) to match recent specification changes and reduce redundancy.</li>
              <li><strong class="strong">FLL:</strong> The <strong class="strong">FSS Basic List (FSS-0002)</strong> to match recent specification changes regading Objects.</li>
              <li><strong class="strong">FLL:</strong> The <strong class="strong">FSS-0002</strong> and <strong class="strong">FSS-0003</strong> standards, modifying the space after Object rules.</li>
              <li><strong class="strong">FLL:</strong> Clarify the <strong class="strong">FSS-0002</strong> and <strong class="strong">FSS-0003</strong> standards regarding the white space before and after a valid Object.</li>
              <li><strong class="strong">Controller:</strong> Cgroup example controller script example to show running under a SystemD system.</li>
              <li><strong class="strong">Firewall:</strong> Refresh firewall iptables rules and relax some of the defaults.</li>
              <li><strong class="strong">FSS Read programs:</strong> Implement Object alignment and trim expanding in <abbr title="Featureless Settings Specifications">FSS</abbr> read programs and restructure the program flags.</li>
              <li><strong class="strong">FSS Read programs:</strong> Always count any <code class="code">--select</code> when using <code class="code">--object</code> for <abbr title="Featureless Settings Specifications">FSS</abbr> read programs.</li>
              <li><strong class="strong">FSS Read programs:</strong> Improvements around <abbr title="Featureless Settings Specifications">FSS</abbr> read functions regarding correctness.</li>
              <li><strong class="strong">FSS Read programs:</strong> Back port tests for the <abbr title="Featureless Settings Specifications">FSS</abbr> read programs with more recent changes.</li>
            </ul>
            <p class="p">
              Check out the project on the <a href="fll.html#release" class="link"><abbr title="Featureless Linux Library">FLL</abbr> release</a> page.
            </p>
            <p class="p">
              The project is built like a tool chest.
              Individuals who are unfamiliar with the project should have an easier time trying out the pre-packaged <a href="https://sourceforge.net/projects/fll/files/FLL-0.6/0.6.12/monolithic/" class="link external">monolithic sources</a>.
              These <a href="https://sourceforge.net/projects/fll/files/FLL-0.6/0.6.12/programs/" class="link external">programs</a>, by default, are designed to build against a library built using the monolithic source tree.
            </p>
            <p class="p">
              The <strong class="strong">stand alone</strong> sources for the several programs can be found <a href="https://sourceforge.net/projects/fll/files/FLL-0.6/0.6.12/stand_alone/" class="link external">pre-packaged at Sourceforge</a>.
            </p>
            <p class="p">
              <strong class="strong">Kevin Day</strong>
            </p>
          </div>
        </main>
      </div>
    </div>
  </body>
</html>
